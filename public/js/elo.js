function updateRating(players, game) {

    // calculate average ratings of each team
    var r_a = (players[0][0].rating.elo + players[0][1].rating.elo) / 2;
    var r_b = (players[1][0].rating.elo + players[1][1].rating.elo) / 2;
    var diff = r_b -r_a;

    // limit diff
    if (diff > 400) diff = 400;
    if (diff < -400) diff = -400;

    // expected result
    var expected = 1 / (1 + Math.pow(10, (diff) / 400));
    var result = 0.5;
    if (parseInt(game.result[0]) > parseInt(game.result[1])) result = 1;
    if (parseInt(game.result[0]) < parseInt(game.result[1])) result = 0;
    var raw_delta = result - expected;

    // update rating
    updatePlayer(game.players[0][0], players[0][0], raw_delta);
    updatePlayer(game.players[0][1], players[0][1], raw_delta);
    updatePlayer(game.players[1][0], players[1][0], - raw_delta);
    updatePlayer(game.players[1][1], players[1][1], - raw_delta);

}

function updatePlayer(gamePlayer, player, raw_delta) {

    if (player.games < 10) {
        player.rating.k = 40;
    } else {
        player.rating.k = 20
    }

    gamePlayer.name = player.name;
    gamePlayer.delta = Math.round(player.rating.k * raw_delta);
    gamePlayer.rating = Math.round(player.rating.elo);

    player.status = "active";
    player.games = player.games + 1;
    player.rating.elo = Math.round(player.rating.elo + gamePlayer.delta);
}

function revertRating(players, game) {
    revertPlayer(game.players[0][0], players[0][0]);
    revertPlayer(game.players[0][1], players[0][1]);
    revertPlayer(game.players[1][0], players[1][0]);
    revertPlayer(game.players[1][1], players[1][1]);
}

function revertPlayer(gamePlayer, player) {
    player.rating.elo = gamePlayer.rating;
    player.games -= 1;

    if (player.games <= 10) {
        player.rating.k = 40;
    } else {
        player.rating.k = 20
    }
}

