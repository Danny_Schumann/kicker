var BASE_URL = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');
//var BASE_URL = "http://localhost:3000";

// bootstrap the demo
var vueModel = new Vue({
    el: '#main',
    created() {
        this.pollPlayersAndGames();
    },
    data: {
        players: [],
        chartPlayerIds : [],
        games: [],
        games_limit: 1000,
        goals: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        new_player: '',
        new_game: {
            "players": [[
                {
                    "_id": "",
                    "name": "",
                    "rating": 0,
                    "delta": 0
                },
                {
                    "_id": "",
                    "name": "",
                    "rating": 0,
                    "delta": 0
                }
            ], [
                {
                    "_id": "",
                    "name": "",
                    "rating": 0,
                    "delta": 0
                },
                {
                    "_id": "",
                    "name": "",
                    "rating": 0,
                    "delta": 0
                }
            ]],
            "result": [0, 0]
        },
        trends:{
            up_strong : 40,
            up_light: 5,
            down_light: -5,
            down_strong: -40
        }
    },
    methods: {
        pollPlayersAndGames() {
            $.snackbar({content: 'Load players and games each 60 seconds ...'});
            this.fetchPlayers();
            this.fetchGames();
            setTimeout(this.pollPlayersAndGames, 60000);
        },
        fetchPlayers() {
            var url = BASE_URL + '/players';
            axios.get(url).then(response => {
                response.data.sort(function (a, b) {
                    // sort players with stable rating before all others
                    if (b.rating.k != a.rating.k) {
                        return a.rating.k - b.rating.k;
                    }

                    // sort players with same rating stability by rating
                    return b.rating.elo - a.rating.elo;
                })
                this.players = response.data;
            });
        },
        getPlayerById(id) {
            for (var i in this.players) {
                if (this.players[i]._id == id) {
                    return this.players[i];
                }
            }
            return null;
        },
        getPlayersFromIds(inGamePlayers) {
            var p1a = this.getPlayerById(inGamePlayers[0][0]._id);
            var p2a = this.getPlayerById(inGamePlayers[0][1]._id);
            var p1b = this.getPlayerById(inGamePlayers[1][0]._id);
            var p2b = this.getPlayerById(inGamePlayers[1][1]._id);

            if (p1a == null || p2a == null || p1b == null || p2b == null) {
                return null;
            }

            // check for duplicate players
            if (p1a._id == p2a._id || p1a._id == p1b._id || p1a._id == p2b._id ||
                p2a._id == p1b._id || p2a._id == p2b._id || p1b._id == p2b._id) {
                return null;
            }

            return [[p1a, p2a], [p1b, p2b]];
        },
        addPlayer() {
            var url = BASE_URL + '/players';
            axios.post(url, {name: this.new_player}).then(response => {
                this.fetchPlayers();
            });
        },
        updatePlayer(player) {
            var url = BASE_URL + '/players/' + player._id;
            axios.put(url, player).then(response => {
                // do nothing...
            });
        },
        fetchGames() {
            var url = BASE_URL + '/games?limit=' + this.games_limit;
            axios.get(url).then(response => {
                this.games = response.data;
            });
        },
        addGame() {

            var players_of_game = this.getPlayersFromIds(this.new_game.players);

            if (players_of_game == null) {
                $.snackbar({content: 'ERROR: Failed to add game, because of invalid paring'});
                return;
            }

            $.snackbar({content: 'Add game...'});

            // update rating
            updateRating(players_of_game, this.new_game);

            // update players
            this.updatePlayer(players_of_game[0][0]);
            this.updatePlayer(players_of_game[0][1]);
            this.updatePlayer(players_of_game[1][0]);
            this.updatePlayer(players_of_game[1][1]);

            // update game
            var url = BASE_URL + '/games';
            axios.post(url, this.new_game).then(response => {
                $.snackbar({content: 'Added'});
                this.fetchPlayers();
                this.fetchGames();
            });
        },
        undoLastGame() {
            $.snackbar({content: 'Revert last game...'});
            var game = this.games[0];

            var players_of_game = this.getPlayersFromIds(game.players);

            if (players_of_game == null) {
                $.snackbar({content: 'ERROR: Failed to revert game, because of invalid paring'});
                return;
            }

            revertRating(players_of_game, game);

            // update players
            this.updatePlayer(players_of_game[0][0]);
            this.updatePlayer(players_of_game[0][1]);
            this.updatePlayer(players_of_game[1][0]);
            this.updatePlayer(players_of_game[1][1]);

            // delete game
            var url = BASE_URL + '/games/'+game._id;
            axios.delete(url, this.game).then(response => {
                $.snackbar({content: 'Reverted last game'});
                this.fetchPlayers();
                this.fetchGames();
            });
        },
        closeChart() {
            // hide chart
            $("#chart").addClass('d-none');
        },
        drawChart(checkPlayer) {
            // show chart
            $("#chart").removeClass('d-none');

            // toggle single player
            if (checkPlayer != null) {
                // add/remove player from chartPlayerIds
                if (this.chartPlayerIds.includes(checkPlayer._id)) {
                    // remove element from array
                    for( var i = 0; i < this.chartPlayerIds.length; i++){
                        if (this.chartPlayerIds[i] === checkPlayer._id) {
                            this.chartPlayerIds.splice(i, 1);
                        }
                    }
                } else {
                    this.chartPlayerIds.push(checkPlayer._id);
                }
                if (this.chartPlayerIds.length == 0) {
                    $("#chart").addClass('d-none');
                    return;
                }


            // toggle all players
            } else {
                if (this.chartPlayerIds.length == this.players.length) {
                    this.chartPlayerIds = [];
                    $("#chart").addClass('d-none');
                    return;
                } else {
                    var ids = [];
                    this.players.forEach(player => {
                        ids.push(player._id);
                    });
                    this.chartPlayerIds = ids;
                }
            }

            console.log(this.chartPlayerIds);

            // clear previous chart.data
            chart.data = {
                labels: [],
                datasets: []
            };
            var label = '';
            this.chartPlayerIds.forEach(function(chartPlayerId, chartIndex) {
                var data = {};
                var lastRating = null;
                var labels = {};
                // copy and reverse games
                var games = [...vueModel.games].reverse();

                games.forEach(game => {
                    var localDateString = new Date(game.created).toLocaleDateString();
                    labels[localDateString] = null; // only key needed

                    game.players.forEach(team => {
                        team.forEach(player => {
                            if (player._id === chartPlayerId) {
                                lastRating = player.rating + player.delta
                                label = player.name;
                            }
                        });
                    });

                    data[localDateString] = lastRating;

                });

                chart.data.labels = Object.keys(labels);
                chart.data.datasets[chartIndex] = {
                    fill: false,
                    data: Object.values(data),
                    label: label,
                    borderColor: pickColor(label),
                    backgroundColor: pickColor(label),
                    borderWidth: 1
                }

            });


            chart.update();

        },
        getFormCurve(checkPlayer) {

            // TODO buffer result for consecutive calls...

            var result = {
                tendency: 0,
                formCurveString: [],
            }

            var num_games = 5;

            // calculate tendency value and form curve
            this.games.forEach(game => {
                var team_id = 0;
                game.players.forEach(team => {
                    var opponent_team_id = 1 - team_id;
                    team.forEach(player => {
                        if (player._id === checkPlayer._id) {
                            var str = "D";
                            if (parseInt(game.result[team_id]) < parseInt(game.result[opponent_team_id])) {
                                str = "L";
                            } else if (parseInt(game.result[team_id]) > parseInt(game.result[opponent_team_id])) {
                                str = "W";
                            }
                            if (result.formCurveString.length < num_games) {
                                result.tendency += player.delta;
                                result.formCurveString.unshift(str)
                            } else {
                                return result;
                            }
                        }
                    });
                    team_id += 1;
                });
            });
            return result;
        }
    }
});

var ctx = document.getElementById('rating-chart-canvas').getContext('2d');
var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [],
        datasets: []
    }
});


function hashCode(str) {
    let hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
}

function pickColor(str) {
    return `hsl(${hashCode(str) % 360}, 100%, 50%)`;
}

$(document).ready(function() {
    // init bootstrap
    $('body').bootstrapMaterialDesign();
});
