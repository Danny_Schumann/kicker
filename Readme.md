# Kicker

Kicker is a table football rating server using the ELO rating system for 2 vs 2 games.

## Quickstart

The application requires Nodejs and a MongoDB database "kickerdb".

Run the server.

```
nodejs server.js
```
Open user interface at http://localhost:3000.

![Screenshot](.gitlab/kicker-screenshot.PNG?raw=true "Screenshot")

![Screenshot2](.gitlab/kicker-screenshot2.PNG?raw=true "Screenshot2")

## Credits

Gitlab icon made by Freepik from www.flaticon.com