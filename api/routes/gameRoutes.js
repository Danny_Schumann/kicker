'use strict';
module.exports = function(app) {
    var gameController = require('../controllers/gameController');

    // player Routes
    app.route('/games')
        .get(gameController.list_all)
        .post(gameController.create);


    app.route('/games/:gameId')
        .get(gameController.read)
        .put(gameController.update)
        .delete(gameController.delete);
};