'use strict';


var mongoose = require('mongoose'),
    Game = mongoose.model('Game');

exports.list_all = function(req, res) {
    Game.find({})
        .sort({_id: -1})
        .limit(parseInt(req.query.limit))
        .exec(function(err, games) {
        if (err)
            res.send(err);
        res.json(games);
    });
};




exports.create = function(req, res) {
    var new_game = new Game(req.body);
    new_game.save(function(err, game) {
        if (err)
            res.send(err);
        res.json(game);
    });
};


exports.read = function(req, res) {
    Game.findById(req.params.gameId)
        .exe(function(err, game) {
            if (err)
                res.send(err);
            res.json(game);
    });
};


exports.update = function(req, res) {
    Game.findOneAndUpdate({_id: req.params.gameId}, req.body, {new: true}, function(err, game) {
        if (err)
            res.send(err);
        res.json(game);
    });
};


exports.delete = function(req, res) {

    Game.remove({
        _id: req.params.gameId
    }, function(err, task) {
        if (err)
            res.send(err);
        res.json({ message: 'Game successfully deleted' });
    });
};
