'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var GameSchema = new Schema({

    players: {
        type:
        [[
            {
                _id: {
                    type: Schema.Types.ObjectId,
                    required: 'id required'
                },
                name: {
                    type: String,
                },
                rating: {
                    type: Number,
                    default: 1500
                },
                delta: {
                    type: Number,
                    default: 0
                }
            }
        ]],
        validate: [validatePlayers, '2 x 2 players matrix required']
    },
    result: {
        type: [
            {
                type: Number,
                default: 0
            }
        ],
        validate: [validateResults, "result size must be 2"]
    },

    created: {
        type: Date,
        default: Date.now
    }
});

function validatePlayers(val) {
    return val.length == 2 && val[0].length == 2 && val[1].length == 2;
}

function validateResults(val) {
    return val.length == 2;
}

module.exports = mongoose.model('Game', GameSchema);
