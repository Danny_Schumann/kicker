'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var PlayerSchema = new Schema({
    name: {
        type: String,
        required: 'User name required'
    },
    rating: {
        elo: {
            type: Number,
            default: 1500
        },
        k: {
            type: Number,
            default: 50
        }
    },
    games: {
        type: Number,
        default: 0
    },
    created: {
        type: Date,
        default: Date.now
    },
    status: {
        type: [{
            type: String,
            enum: ['new', 'active', 'inactive']
        }],
        default: ['new']
    }
});

module.exports = mongoose.model('Player', PlayerSchema);
