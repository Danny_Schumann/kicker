var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    cors = require('cors'),
    mongoose = require('mongoose'),
    Player = require('./api/models/playerModel'), //created model loading 
    Game = require('./api/models/gameModel'), //created model loading here
    bodyParser = require('body-parser');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/kickerdb', { useNewUrlParser: true });

app.use(express.static('public'));

//make way for some custom css, js and images
app.use('/css', express.static(__dirname + '/public/css'));
app.use('/js', express.static(__dirname + '/public/js'));
app.use('/images', express.static(__dirname + '/public/images'));

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var playerRoutes = require('./api/routes/playerRoutes'); //importing route
playerRoutes(app); //register the route

var gameRoutes = require('./api/routes/gameRoutes'); //importing route
gameRoutes(app); //register the route

app.listen(port);

console.log('Kicker RESTful API server started on: ' + port);
